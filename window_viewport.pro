#-------------------------------------------------
#
# Project created by QtCreator 2013-11-07T15:56:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = window_viewport
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    paintedwidget.cpp

HEADERS  += widget.h \
    paintedwidget.h

FORMS    += widget.ui
