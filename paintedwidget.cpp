#include "paintedwidget.h"
#include <QPainter>
#include <QDebug>

PaintedWidget::PaintedWidget(QWidget *parent) :
    QWidget(parent)
{
    resize(200, 200);
    setWindowTitle("Paint Demo");
}

void PaintedWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    //painter.setWindow(-50, -50, 100, 100);
    //painter.drawRect(-50, -50, 50, 50);
    //painter.drawRect(0, 0, 50, 50);

    qDebug() << "after drag:" << endl;
    qDebug() << painter.viewport().width();
    qDebug() << painter.viewport().height();

    painter.setViewport(0, 0, 100, 100);
    painter.setWindow(-50, -50, 100, 100);
    painter.drawRect(0, 0, 100, 100);
}
